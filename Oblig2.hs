{-# OPTIONS_GHC -Wincomplete-patterns #-}
-- Jonas Valen
-- Group 3
-- Glider: 10 (s 0 0, b 2 2) 1 1 2 2 3 2 4 1

import Data.Char
import Data.List
import Data.Maybe
import Data.Foldable
import Control.Exception
import Control.Concurrent
import Text.Read

type Game = (Size, Board, Rules)
type Size = Int
type Board = [Position]
type Position = (Int, Int)
type Rules = (Survive, Born)
type Survive = Rule
type Born = Rule
type Rule = (Int, Int)

main :: IO ()
main = do
        clear
        loop (-1, [], ((0, 0), (0, 0)))

-- | Commands:
--  c n – create and show a new, empty board of size n × n. (n :: Int and 1 ≤ n ≤ 99)
--  n x1 y1 ... xk yk – place k living cells at (x1 , y1 ), ..., (xk, yk ).
--  e x1 y1 ... xk yk – make positions (x1 , y1 ), ..., (xk , yk ) empty.
--  s m n – Set the rules determining if a cell survives.
--  b m n – Set the rules determining if a cell is born.
--  r name - Read from file: n (s m n, b m n) x1 y1 ... xk yk.
--  l x – Enter live mode for x turns.
--  Enter/Return button with empty input, progresses one generation.
--  w – Shows the positions of all living cells currently on the board.
--  ? – Shows the rules of the current game.
--  quit – quits the program.
commands :: Game -> String -> IO ()
commands (size, b, r) [] = cmdL (size, b, r) "1"
commands (size, b, r) "quit" = outputMsg size "Quitting ... \n"
commands (size, b, r) (cmd : [])
      | (cmd == '?' || cmd == 'w') = do
            let msg = if cmd == 'w' then "Living cells: " ++ tail(reverse(tail(reverse (show b))))
                  else "Survival (min,max): "++show(fst r)++", Born (min,max): "++show (snd r)
            outputError (size, b, r) msg
      | otherwise = outputError (size, b, r) "Invalid command."
commands (size, b, r) (cmd : args)
      | cmd == 'c' = cmdC (size, b, r) args
      | cmd == 'n' || cmd == 'e' = cmdNE (size, b, r) args cmd
      | cmd == 's' || cmd == 'b' = cmdSB (size, b, r) args cmd
      | cmd == 'r' && args /= [] = cmdR (size, b, r) args
      | cmd == 'l' && args /= [] = cmdL (size, b, r) args
      | otherwise = outputError (size, b, r) "Invalid command."

---- **** Command Helpers **** ----
-- | Prompt loop
loop :: Game -> IO ()
loop (size, b, r) = do
            outputPrompt size
            cmd <- getLine
            outputMsg size ""
            commands (size, b, r) cmd

-- | Helper for commands case 'c'.
cmdC :: Game -> String -> IO ()
cmdC (size, b, r) args = case readMaybe args :: Maybe Int of
      Just num -> case (num > 0 && num < 100) of
            False -> outputError (size, b, r) "Board size must be minimum 1 and maximum 99."
            True -> do
                  clear
                  board num
                  loop (num, [], r)
      Nothing -> outputError (size, b, r) "Invalid argument. Argument must be a number."

-- | Helper for command case 'n' and 'e'.
cmdNE :: Game -> String -> Char -> IO ()
cmdNE (size, b, r) args cmd
      | size < 1 = outputError (size, b, r) "Create or load a board to add/remove cells."
      | otherwise = do
            let pairs = getPairs args
            case length pairs of
                  0 -> outputError (size, b, r) "Invalid argument. Argument must be coordinates."
                  more -> case inBounds pairs size 1 of
                        False -> outputError (size, b, r) "Coordinates out of bounds."
                        True -> if cmd == 'n' then do
                                    forM_ pairs $ \p -> outputAt (convXY p) "0"
                                    loop (size, union b pairs, r)
                              else do
                                    forM_ pairs $ \p -> outputAt (convXY p) "."
                                    loop (size, b \\ pairs, r)

-- | Helper for commands 's' and 'b'.
cmdSB :: Game -> String -> Char -> IO ()
cmdSB (size, b, r) args cmd = do
      let ps = getPairs args
      if length ps == 1 then do
            let p = head ps
            if cmd == 's' && fst p <= snd p
                  then loop (size, b, ((fst p, snd p), snd r))
            else if cmd == 'b' && fst p <= snd p then loop (size, b, (fst r, (fst p, snd p)))
                  else outputError (size, b, r) "Invalid format. Format m n where 0 <= m <= n."
      else outputError (size, b, r) "Invalid argument. Argument must be two numbers"

-- | Helper for command 'r'.
cmdR :: Game -> String -> IO ()
cmdR (size, b, r) args = do
      file <- try (readFile $ tail args) :: IO (Either SomeException String)
      case file of
            Left except -> outputError (size, b, r) "File not found!"
            Right fileLine -> handleFile (size, b, r) fileLine

-- | Helper for command 'l'.
cmdL :: Game -> String -> IO ()
cmdL (size, b, r) args = do
      case readMaybe args :: Maybe Int of
            Just turns -> if size > 0 then liveMode (size, b, r) [1..turns]
                  else outputError (size, b, r) "Create or load a map to use livemode."
            Nothing -> outputError (size, b, r) "Invalid argument. Argument must be a number."


---- **** Game functions **** ----
-- | Helper for cmdL. Runs live mode.
liveMode :: Game -> [Int] -> IO ()
liveMode (size, b, r) [] = loop (size, b, r)
liveMode (size, b, r) (turn : turns) = do
      let newBoard = nextTurn (size, b, r)
      let same = newBoard `intersect` b
      if (length same == length newBoard && length same == length b)
            then outputError (size, b, r) "Stable configuration reached!"
            else do
                    let rem = b \\ newBoard
                    forM_ rem $ \p -> outputAt (convXY p) "."
                    let add = newBoard \\ b
                    forM_ add $ \p -> outputAt (convXY p) "0"
                    threadDelay 200000
                    liveMode (size, newBoard, r) turns

-- | Wrapper for checkCells.
nextTurn :: Game -> Board
nextTurn (size, board, (s, b))
      | fst b == 0 = checkCells (size, board, (s, b)) (getAllPositions size)
      | otherwise  = checkCells (size, board, (s, b)) (getNeighbors size board)

-- | Checks for surviving and newborn cells.
checkCells :: Game -> Board -> Board
checkCells (size, b, r) [] = []
checkCells (size, board, (s, b)) (p : ps)
      | p `elem` board = if aliveNext size board s p
            then p : checkCells (size, board, (s, b)) ps
            else checkCells (size, board, (s, b)) ps
      | otherwise = if aliveNext size board b p
            then p : checkCells (size, board, (s, b)) ps
            else checkCells (size, board, (s, b)) ps

-- | Checks if a cell will be alive next round
aliveNext :: Size -> Board -> Rule -> Position -> Bool
aliveNext size board (min, max) pos =
      length (intersect (getNeighbors size [pos]) board) <= max && length (intersect (getNeighbors size [pos]) board) >= min

-- | Finds neighbors in bounds for all coordinates.
getNeighbors :: Size -> Board -> Board
getNeighbors size [] = []
getNeighbors size ((x, y) : ps) = getNeighbors size ps `union`
      [(x', y') | x' <- [(x-1)..(x+1)], y' <- [(y-1)..(y+1)], (x',y') /= (x,y) && x' <= size && x' >= 1 && y' <= size && y' >= 1]

-- | Lists all coordinates in a board .
getAllPositions :: Size -> Board
getAllPositions size = [(x,y) | x <- [1..size], y <- [1..size]]


---- **** Input Helpers **** ----
-- | Helper for cmdR. Parses and handles exceptions from parsing file input.
handleFile :: Game -> String -> IO ()
handleFile (size, b, r) fileLine = do
      let tokens = tokenize fileLine
      case readMaybe $ head tokens :: Maybe Int of
            Just size -> case elemIndex "s" tokens of
                  Just indexS -> case elemIndex "b" tokens of
                        Just indexB -> do
                              let indexLive = if indexS > indexB then indexS + 3 else indexB + 3
                              let pairs = makePairs $ snd $ splitAt indexLive tokens
                              clear
                              board size
                              forM_ pairs $ \p -> outputAt (convXY p) "0"
                              loop (size, pairs, (head $ makePairs [tokens !! (indexS + 1), tokens !! (indexS + 2)],
                                    head $ makePairs [tokens !! (indexB + 1), tokens !! (indexB + 2)]))
                        Nothing -> outputError (size, b, r) "File should include rules for cells being born."
                  Nothing -> outputError (size, b, r) "File should include rules for cell survival."
            Nothing -> outputError (size, b, r) "First element should be a number."

-- | Helper for handleFileTokenizer.
tokenize :: String -> [String]
tokenize [] = []
tokenize ('\n' : str) = tokenize str
tokenize (' ' : str)  = tokenize str
tokenize ('(' : str)  = tokenize str
tokenize (')' : str)  = tokenize str
tokenize (',' : str)  = tokenize str
tokenize (c : str)
            | isDigit c = let (num, left)   = span isDigit str in (c : num) : tokenize left
            | isAlpha c = let (word, left)  = span isAlpha str in (c : word) : tokenize left
            | otherwise = []

-- | Takes a String and returns a list of (x, y) coordinates if the string only contains digits and spaces.
getPairs :: String -> [(Int, Int)]
getPairs str = if (check " 0123456789" str && length str > 0)
                     then do let strs = words str
                             if length strs `mod` 2 == 0 then makePairs strs
                             else []
                     else []

-- | Turns words into pairs of (Int, Int)
makePairs :: [String] -> [(Int, Int)]
makePairs [] = []
makePairs (s1 : s2 : rest) = (read s1 :: Int, read s2 :: Int) : makePairs rest
makePairs [_] = []

-- | Helper for getPairs.
check :: String -> String -> Bool
check str [] = True
check str (c : cs) = (any (c ==) str) && check str cs

-- | Checks if coordinates are in bounds.
inBounds :: [Position] -> Int -> Int -> Bool
inBounds [] max min = True
inBounds (p : ps) max min = fst p <= max && fst p >= min && snd p <= max && snd p >= min && inBounds ps max min

-- | Converts X and Y coordinates to screen
convXY :: Position -> Position
convXY (x, y) = ((3 * x + 1), (y + 1))


---- **** Output Helpers **** ----
-- | Prints message and returns to command loop
outputError :: Game -> String -> IO ()
outputError (size, b, r) str = do
      outputMsg size str
      loop (size, b, r)

-- | Prints message to screen
outputMsg :: Size -> String -> IO ()
outputMsg size str = do
      goto (1, (size + 3))
      clearLine
      outputAt (1, (size + 3)) str

-- | Printer function
outputAt :: Position -> String -> IO ()
outputAt (x, y) str = do
      goto (x, y)
      putStr str

-- | Prints the command prompt
outputPrompt :: Size -> IO ()
outputPrompt size = do
      goto(1, (size + 2))
      clearLine
      outputAt(1, (size + 2)) "The Game of Life $ "

-- | Moves the cursot to a screen coordinate
goto :: Position -> IO ()
goto (x, y) = putStr ("\ESC[" ++ show y ++ ";"++ show x ++ "H")

-- | Clears line
clearLine :: IO ()
clearLine = putStr "\ESC[K"

-- | Clears terminal
clear :: IO ()
clear = do
      putStr "\ESC[2J\ESC[3J"
      goto (0,0)


---- **** Board functions **** ----
-- | Prints board grid of size n x n.
board :: Size -> IO ()
board size = putStr ( "  " ++ top [1..size] ++ line [1..size]) where
      line :: [Int] -> String
      line [] = ""
      line (x:xs) | x >= 10 = show x ++ space [1..size] ++ "\n" ++  line xs
                  | x < 10 = " " ++ show x ++ space [1..size] ++ "\n" ++ line xs

-- | Support method for grid to print row 0 though n
top :: [Int] -> String
top [] = "\n"
top (y:ys) | y < 10  = " " ++ show(y) ++ " " ++ top ys
           | y >= 10 = show(y) ++ " " ++ top ys

-- | Support method for grid to add correct spacing.
space :: [Int] -> String
space [] = ""
space (y:ys) | y < 10   = " . " ++ space ys
             | y >= 10  = " . " ++ space ys